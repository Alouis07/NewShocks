import sys, os
sys.path.insert(0, os.path.abspath('..'))
from lib.libbtaps import BTaps

addr = sys.argv[1]
btapsobj = BTaps(addr)
if btapsobj.connect() == 1:
    state , timers = btapsobj.get_switch_state()
    btapsobj.set_switch(not state)
    btapsobj.disconnect()
 
