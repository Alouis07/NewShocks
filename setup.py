
import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "GRWM",
    author = "Newshocks Senior Design Team",
    author_email = "",
    version = "0.1",
    url = "https://gitlab.com/Alouis07/NewShocks",
    license = "",
    description = "Gesture ",
    long_description = read('README.md'),
    keywords = "",
    packages=find_packages(),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
)
