import bluetooth
import subprocess
from lib.PyoConnectLib import *

class NSMyo(Myo):
    def __init__(self, cls, tty = None):
        self.setLockingPolicy('none')
        Myo.__init__(self, tty)
        self.isMyoConnected = False
        self.ConnectMyo()
    def Lock(self):
        if self.isUnlocked():
            myo.lock()
            print("Device Locked")
        else:
            print('Error: Device already locked')
    def Unlock(self):
        if self.isUnlocked():
            print('Error: Device already unlocked')
        else:
            self.unlock("hold")
            print("Device Unlocked")
    def ConnectMyo(self):
        if self.isMyoConnected:
            self.disconnect()
            time.sleep(1)
        self.connect()
        self.isMyoConnected = True
    def DisconnectMyo(self):
        if self.isMyoConnected:
            self.disconnect()
            self.isMyoConnected = False
            print('Myo disconnected')
        else:
            print('Myo not connected')
    def IsDoubleTap(self):
        currentpose = self.getPose()
        if currentpose == 'doubleTap':
            return True
        else:
            return False
    def IsFist(self):
        currentpose = self.getPose()
        if currentpose == 'fist':
            return True
        else:
            return False
    def GetPose(self):
        return self.getPose()
    
    


class PlugableDriver:
    def __init__(self):
        #self.p = subprocess.Popen(['python3', 'NewShock.scan()'])
        #print("Scanner started with PID", self.p.pid)
        self.dev_list = []
        self.dev_list = self.get_devs()
        
        
    def get_devs(self):
        
        with open("dev_list.dat") as f:
            try:
                self.dev_list = eval(f.read())
            except:
                self.dev_list.sort(key=lambda tup: tup[1])# sorts list by name
        return self.dev_list

    def scan(self):
        while True:
            dev_list = bluetooth.discover_devices(lookup_names = True)
            with open("dev_list.dat", "w") as f:
                f.write(repr(dev_list))
        
    def quitscan(self):
        self.p.terminate()
        print("Stopping Scanner")
        
