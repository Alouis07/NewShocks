
# Written by Timothy Michael for Newshocks Project

import sys, os
sys.path.insert(0, os.path.abspath('..'))
import tkinter as tk
from tkinter import ttk
from functools import partial

import time
try:
    import keyboard
except:
    print("Keyboard use: Execute with sudo permission or give user access to devids") 



from lib.NewShock import *


global devs
global dev_index


btn_timer = []
dev_index = 0
new_index_flag = False
btapsobj = []
btns = []
devs = []
new_devices = True
btnstate = []#Will be used to advance selections through menu
status = []



def Close():
    global pleaseQuit
    global bTaps
    global root
    global myo
    try:
        bTaps.quitscan()
    except:
        print("Error: No Scanner to close") 

    try:
        root.destroy()
    except:
        print("App window already closed")

    try:
        myo.DisconnectMyo()
        print("Myo Disconnected")
    except:
        print("Myo already disconnected")
    finally:
        print("Processes terminated")
        sys.exit()
        
            
def AddPlugableBtn(btnframe): #function for adding dynamic buttons
    global btns
    if devs:
        for btn_old in btns:
            btn_old.destroy()
        btns.clear()
        btns = []
        print("dev list:", devs)
        for index in range(len(devs)):
            addr, name = devs[index]               
            btn = tk.Button(btnframe,text=name,command= partial(ActivateButton, index),height = 3, width = 20)
            btn.pack()
            btns.append(btn)
        
def ActivateButton(d_index):
    global btn_timer
    if d_index ==-1:
       Close()
    elif devs:
        addr, name = devs[dev_index]
        print("Activating Button:",devs[d_index])
        os.system('python3 Btaps.py %s' %addr)
        print("button activated")
        btn_timer = time.time()
        BtnLocks(True)
        
def BtnLocks(arg):
    for btn in btns:
        if arg:
            btn.config(state=tk.DISABLED)
        else:
            btn.config(state=tk.NORMAL)

def BtnSelect():
    d = dev_index + 1
    if d >= len(devs):
        d = -1
    return d

def IluminateButton(arg,eBtn,btns):
    if arg == -1:
        eBtn.config(background='gold')
        btns[len(btns)-1].config(background='#d9d9d9')
    elif arg == 0:
        btns[arg].config(background='gold')
        eBtn.config(background='#d9d9d9')
    else:
        btns[arg-1].config(background='#d9d9d9')
        btns[arg].config(background='gold')



            
root = tk.Tk()
root.title('NewShocks')
pb = ttk.Progressbar(root, orient="vertical", length=480, mode="determinate")
pb.pack(side=tk.LEFT)
main = tk.Frame(root, width=800, height=480, background='gray95')
main.pack(fill=tk.BOTH, expand=1)
topframe = tk.Frame(main, width=780, height=90, padx=10, pady=10, background='gray20')
topframe.pack_propagate(0)
exitbtn = tk.Button(topframe, text='Quit', border=0, command=Close, height = 20, width = 10)
exitbtn.pack(side=tk.RIGHT)
topframe.pack(fill=tk.BOTH)
toplabel = tk.Label(topframe, text='NewShock Gesture Recognition Device', background='gray20', foreground='#50BBE7', font='Arial 20')
toplabel.pack(fill=tk.BOTH)
btnframe = tk.Frame(main, width=780, padx=10, pady=10, background='gray95')
btnframe.pack(fill=tk.X)
tk.Label(btnframe, text='Bluetooth Devices', background='gray95').pack()


 
#myo = NSMyo(sys.argv[1] if len(sys.argv) >= 2 else None)


bTaps = PlugableDriver()
devs = bTaps.get_devs()
pleaseQuit = False


scan_timer = 0
select_timer = time.time()

fistcount = 0
while not pleaseQuit:
    if keyboard.is_pressed('q') or keyboard.is_pressed('ESC'):#if key 'q' or <Escape> is pressed start exits commands
        print('Escape or q Pressed')
        Close()
    if keyboard.is_pressed('f'):
        root.attributes('-fullscreen', True)


        
#    if myo.isMyoConnected:
#        p = myo.run(1.0)
#        if time.time() - t0 > 0.3:
#            myo.DisconnectMyo()
#        else:
#            myo.tick()
#            toplabel.config(text=myo.GetPose())
    
    t0 = time.time()
    if abs(scan_timer - t0) > 10:
         AddPlugableBtn(btnframe)
         #add bluetooth class scanner
         scan_timer = t0
         
    if select_timer:
        if abs(select_timer - t0) > .5:
            if keyboard.is_pressed('space') or keyboard.is_pressed('up'):# or myo.IsDoubleTap():
                dev_index = BtnSelect()
                select_timer=time.time()
        
            if keyboard.is_pressed('enter'):
                ActivateButton(dev_index)
                select_timer = time.time()
            if keyboard.is_pressed('x'):# or myo.IsFist():
                pb.step(1)
                if fistcount <= 0:
                    fistcount = 1
                else:
                    fistcount = fistcount + 1
            else:
                pb.config(value = 0)
                fistcount = 0
            if fistcount >= 100:
                ActivateButton(dev_index)
                select_timer = time.time()
                fistcount = 0

    
    
    IluminateButton(dev_index,exitbtn,btns)
    if btn_timer:
        if abs(btn_timer - t0) > 10:
            BtnLocks(False)
            btn_timer = []


    try:
        root.update()
    except:
        #print("tkinter object does not exist!")
        pass

        
Close()
